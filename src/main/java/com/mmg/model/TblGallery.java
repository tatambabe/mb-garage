package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_gallery database table.
 * 
 */
@Entity
@Table(name="tbl_gallery")
public class TblGallery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_gallery")
	private Long idGallery;

	@Column(name="pic_name")
	private String picName;

	@Column(name="pic_path")
	private String picPath;

	@Column(name="pic_type")
	private String picType;

	//bi-directional many-to-one association to TblCarseat
	@OneToMany(mappedBy="tblGallery")
	private List<TblCarseat> tblCarseats;

	//bi-directional many-to-one association to TblHandbar
	@OneToMany(mappedBy="tblGallery")
	private List<TblHandbar> tblHandbars;

	//bi-directional many-to-one association to TblHisgallery
	@OneToMany(mappedBy="tblGallery")
	private List<TblHisgallery> tblHisgalleries;

	//bi-directional many-to-one association to TblTank
	@OneToMany(mappedBy="tblGallery")
	private List<TblTank> tblTanks;

	//bi-directional many-to-one association to TblWheel
	@OneToMany(mappedBy="tblGallery")
	private List<TblWheel> tblWheels;

	public TblGallery() {
	}

	public Long getIdGallery() {
		return this.idGallery;
	}

	public void setIdGallery(Long idGallery) {
		this.idGallery = idGallery;
	}

	public String getPicName() {
		return this.picName;
	}

	public void setPicName(String picName) {
		this.picName = picName;
	}

	public String getPicPath() {
		return this.picPath;
	}

	public void setPicPath(String picPath) {
		this.picPath = picPath;
	}

	public String getPicType() {
		return this.picType;
	}

	public void setPicType(String picType) {
		this.picType = picType;
	}

	public List<TblCarseat> getTblCarseats() {
		return this.tblCarseats;
	}

	public void setTblCarseats(List<TblCarseat> tblCarseats) {
		this.tblCarseats = tblCarseats;
	}

	public TblCarseat addTblCarseat(TblCarseat tblCarseat) {
		getTblCarseats().add(tblCarseat);
		tblCarseat.setTblGallery(this);

		return tblCarseat;
	}

	public TblCarseat removeTblCarseat(TblCarseat tblCarseat) {
		getTblCarseats().remove(tblCarseat);
		tblCarseat.setTblGallery(null);

		return tblCarseat;
	}

	public List<TblHandbar> getTblHandbars() {
		return this.tblHandbars;
	}

	public void setTblHandbars(List<TblHandbar> tblHandbars) {
		this.tblHandbars = tblHandbars;
	}

	public TblHandbar addTblHandbar(TblHandbar tblHandbar) {
		getTblHandbars().add(tblHandbar);
		tblHandbar.setTblGallery(this);

		return tblHandbar;
	}

	public TblHandbar removeTblHandbar(TblHandbar tblHandbar) {
		getTblHandbars().remove(tblHandbar);
		tblHandbar.setTblGallery(null);

		return tblHandbar;
	}

	public List<TblHisgallery> getTblHisgalleries() {
		return this.tblHisgalleries;
	}

	public void setTblHisgalleries(List<TblHisgallery> tblHisgalleries) {
		this.tblHisgalleries = tblHisgalleries;
	}

	public TblHisgallery addTblHisgallery(TblHisgallery tblHisgallery) {
		getTblHisgalleries().add(tblHisgallery);
		tblHisgallery.setTblGallery(this);

		return tblHisgallery;
	}

	public TblHisgallery removeTblHisgallery(TblHisgallery tblHisgallery) {
		getTblHisgalleries().remove(tblHisgallery);
		tblHisgallery.setTblGallery(null);

		return tblHisgallery;
	}

	public List<TblTank> getTblTanks() {
		return this.tblTanks;
	}

	public void setTblTanks(List<TblTank> tblTanks) {
		this.tblTanks = tblTanks;
	}

	public TblTank addTblTank(TblTank tblTank) {
		getTblTanks().add(tblTank);
		tblTank.setTblGallery(this);

		return tblTank;
	}

	public TblTank removeTblTank(TblTank tblTank) {
		getTblTanks().remove(tblTank);
		tblTank.setTblGallery(null);

		return tblTank;
	}

	public List<TblWheel> getTblWheels() {
		return this.tblWheels;
	}

	public void setTblWheels(List<TblWheel> tblWheels) {
		this.tblWheels = tblWheels;
	}

	public TblWheel addTblWheel(TblWheel tblWheel) {
		getTblWheels().add(tblWheel);
		tblWheel.setTblGallery(this);

		return tblWheel;
	}

	public TblWheel removeTblWheel(TblWheel tblWheel) {
		getTblWheels().remove(tblWheel);
		tblWheel.setTblGallery(null);

		return tblWheel;
	}

}