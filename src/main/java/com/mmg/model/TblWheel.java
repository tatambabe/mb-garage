package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_wheel database table.
 * 
 */
@Entity
@Table(name="tbl_wheel")
public class TblWheel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_wheel")
	private Long idWheel;

	@Column(name="price_product")
	private Float priceProduct;

	@Column(name="wheel_brand")
	private String wheelBrand;

	@Column(name="wheel_size")
	private Long wheelSize;

	//bi-directional many-to-one association to TblOrderdetail
	@OneToMany(mappedBy="tblWheel")
	private List<TblOrderdetail> tblOrderdetails;

	//bi-directional many-to-one association to TblGallery
	@ManyToOne
	@JoinColumn(name="id_gallery")
	private TblGallery tblGallery;

	public TblWheel() {
	}

	public Long getIdWheel() {
		return this.idWheel;
	}

	public void setIdWheel(Long idWheel) {
		this.idWheel = idWheel;
	}

	public Float getPriceProduct() {
		return this.priceProduct;
	}

	public void setPriceProduct(Float priceProduct) {
		this.priceProduct = priceProduct;
	}

	public String getWheelBrand() {
		return this.wheelBrand;
	}

	public void setWheelBrand(String wheelBrand) {
		this.wheelBrand = wheelBrand;
	}

	public Long getWheelSize() {
		return this.wheelSize;
	}

	public void setWheelSize(Long wheelSize) {
		this.wheelSize = wheelSize;
	}

	public List<TblOrderdetail> getTblOrderdetails() {
		return this.tblOrderdetails;
	}

	public void setTblOrderdetails(List<TblOrderdetail> tblOrderdetails) {
		this.tblOrderdetails = tblOrderdetails;
	}

	public TblOrderdetail addTblOrderdetail(TblOrderdetail tblOrderdetail) {
		getTblOrderdetails().add(tblOrderdetail);
		tblOrderdetail.setTblWheel(this);

		return tblOrderdetail;
	}

	public TblOrderdetail removeTblOrderdetail(TblOrderdetail tblOrderdetail) {
		getTblOrderdetails().remove(tblOrderdetail);
		tblOrderdetail.setTblWheel(null);

		return tblOrderdetail;
	}

	public TblGallery getTblGallery() {
		return this.tblGallery;
	}

	public void setTblGallery(TblGallery tblGallery) {
		this.tblGallery = tblGallery;
	}

}