package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_handbar database table.
 * 
 */
@Entity
@Table(name="tbl_handbar")
public class TblHandbar implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_handbar")
	private Long idHandbar;

	@Column(name="hand_brand")
	private String handBrand;

	@Column(name="hand_detail")
	private String handDetail;

	@Column(name="price_product")
	private Float priceProduct;

	//bi-directional many-to-one association to TblGallery
	@ManyToOne
	@JoinColumn(name="id_gallery")
	private TblGallery tblGallery;

	//bi-directional many-to-one association to TblOrderdetail
	@OneToMany(mappedBy="tblHandbar")
	private List<TblOrderdetail> tblOrderdetails;

	public TblHandbar() {
	}

	public Long getIdHandbar() {
		return this.idHandbar;
	}

	public void setIdHandbar(Long idHandbar) {
		this.idHandbar = idHandbar;
	}

	public String getHandBrand() {
		return this.handBrand;
	}

	public void setHandBrand(String handBrand) {
		this.handBrand = handBrand;
	}

	public String getHandDetail() {
		return this.handDetail;
	}

	public void setHandDetail(String handDetail) {
		this.handDetail = handDetail;
	}

	public Float getPriceProduct() {
		return this.priceProduct;
	}

	public void setPriceProduct(Float priceProduct) {
		this.priceProduct = priceProduct;
	}

	public TblGallery getTblGallery() {
		return this.tblGallery;
	}

	public void setTblGallery(TblGallery tblGallery) {
		this.tblGallery = tblGallery;
	}

	public List<TblOrderdetail> getTblOrderdetails() {
		return this.tblOrderdetails;
	}

	public void setTblOrderdetails(List<TblOrderdetail> tblOrderdetails) {
		this.tblOrderdetails = tblOrderdetails;
	}

	public TblOrderdetail addTblOrderdetail(TblOrderdetail tblOrderdetail) {
		getTblOrderdetails().add(tblOrderdetail);
		tblOrderdetail.setTblHandbar(this);

		return tblOrderdetail;
	}

	public TblOrderdetail removeTblOrderdetail(TblOrderdetail tblOrderdetail) {
		getTblOrderdetails().remove(tblOrderdetail);
		tblOrderdetail.setTblHandbar(null);

		return tblOrderdetail;
	}

}