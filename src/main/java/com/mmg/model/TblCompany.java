package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_company database table.
 * 
 */
@Entity
@Table(name="tbl_company")
public class TblCompany implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_company")
	private Long idCompany;

	@Column(name="address_company")
	private String addressCompany;

	@Column(name="name_company")
	private String nameCompany;

	@Column(name="tel_company")
	private String telCompany;

	//bi-directional many-to-one association to TblOrder
	@OneToMany(mappedBy="tblCompany")
	private List<TblOrder> tblOrders;

	public TblCompany() {
	}

	public Long getIdCompany() {
		return this.idCompany;
	}

	public void setIdCompany(Long idCompany) {
		this.idCompany = idCompany;
	}

	public String getAddressCompany() {
		return this.addressCompany;
	}

	public void setAddressCompany(String addressCompany) {
		this.addressCompany = addressCompany;
	}

	public String getNameCompany() {
		return this.nameCompany;
	}

	public void setNameCompany(String nameCompany) {
		this.nameCompany = nameCompany;
	}

	public String getTelCompany() {
		return this.telCompany;
	}

	public void setTelCompany(String telCompany) {
		this.telCompany = telCompany;
	}

	public List<TblOrder> getTblOrders() {
		return this.tblOrders;
	}

	public void setTblOrders(List<TblOrder> tblOrders) {
		this.tblOrders = tblOrders;
	}

	public TblOrder addTblOrder(TblOrder tblOrder) {
		getTblOrders().add(tblOrder);
		tblOrder.setTblCompany(this);

		return tblOrder;
	}

	public TblOrder removeTblOrder(TblOrder tblOrder) {
		getTblOrders().remove(tblOrder);
		tblOrder.setTblCompany(null);

		return tblOrder;
	}

}