package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_orderdetail database table.
 * 
 */
@Entity
@Table(name="tbl_orderdetail")
public class TblOrderdetail implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_orderdetail")
	private Long idOrderdetail;

	@Column(name="pay_id")
	private String payId;

	@Column(name="quantity_order")
	private String quantityOrder;

	@Column(name="user_id")
	private String userId;

	//bi-directional many-to-one association to TblOrder
	@OneToMany(mappedBy="tblOrderdetail")
	private List<TblOrder> tblOrders;

	//bi-directional many-to-one association to TblCarseat
	@ManyToOne
	@JoinColumn(name="id_carseat")
	private TblCarseat tblCarseat;

	//bi-directional many-to-one association to TblHandbar
	@ManyToOne
	@JoinColumn(name="id_handbar")
	private TblHandbar tblHandbar;

	//bi-directional many-to-one association to TblTank
	@ManyToOne
	@JoinColumn(name="id_tank")
	private TblTank tblTank;

	//bi-directional many-to-one association to TblWheel
	@ManyToOne
	@JoinColumn(name="id_wheel")
	private TblWheel tblWheel;

	public TblOrderdetail() {
	}

	public Long getIdOrderdetail() {
		return this.idOrderdetail;
	}

	public void setIdOrderdetail(Long idOrderdetail) {
		this.idOrderdetail = idOrderdetail;
	}

	public String getPayId() {
		return this.payId;
	}

	public void setPayId(String payId) {
		this.payId = payId;
	}

	public String getQuantityOrder() {
		return this.quantityOrder;
	}

	public void setQuantityOrder(String quantityOrder) {
		this.quantityOrder = quantityOrder;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<TblOrder> getTblOrders() {
		return this.tblOrders;
	}

	public void setTblOrders(List<TblOrder> tblOrders) {
		this.tblOrders = tblOrders;
	}

	public TblOrder addTblOrder(TblOrder tblOrder) {
		getTblOrders().add(tblOrder);
		tblOrder.setTblOrderdetail(this);

		return tblOrder;
	}

	public TblOrder removeTblOrder(TblOrder tblOrder) {
		getTblOrders().remove(tblOrder);
		tblOrder.setTblOrderdetail(null);

		return tblOrder;
	}

	public TblCarseat getTblCarseat() {
		return this.tblCarseat;
	}

	public void setTblCarseat(TblCarseat tblCarseat) {
		this.tblCarseat = tblCarseat;
	}

	public TblHandbar getTblHandbar() {
		return this.tblHandbar;
	}

	public void setTblHandbar(TblHandbar tblHandbar) {
		this.tblHandbar = tblHandbar;
	}

	public TblTank getTblTank() {
		return this.tblTank;
	}

	public void setTblTank(TblTank tblTank) {
		this.tblTank = tblTank;
	}

	public TblWheel getTblWheel() {
		return this.tblWheel;
	}

	public void setTblWheel(TblWheel tblWheel) {
		this.tblWheel = tblWheel;
	}

}