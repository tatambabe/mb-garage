package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the tbl_rating database table.
 * 
 */
@Entity
@Table(name="tbl_rating")
public class TblRating implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_rc")
	private Long idRc;

	@Column(name="rc_comment")
	private String rcComment;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="rc_datetime")
	private Date rcDatetime;

	@Column(name="rc_rating")
	private BigDecimal rcRating;

	//bi-directional many-to-one association to TblHisgallery
	@OneToMany(mappedBy="tblRating")
	private List<TblHisgallery> tblHisgalleries;

	public TblRating() {
	}

	public Long getIdRc() {
		return this.idRc;
	}

	public void setIdRc(Long idRc) {
		this.idRc = idRc;
	}

	public String getRcComment() {
		return this.rcComment;
	}

	public void setRcComment(String rcComment) {
		this.rcComment = rcComment;
	}

	public Date getRcDatetime() {
		return this.rcDatetime;
	}

	public void setRcDatetime(Date rcDatetime) {
		this.rcDatetime = rcDatetime;
	}

	public BigDecimal getRcRating() {
		return this.rcRating;
	}

	public void setRcRating(BigDecimal rcRating) {
		this.rcRating = rcRating;
	}

	public List<TblHisgallery> getTblHisgalleries() {
		return this.tblHisgalleries;
	}

	public void setTblHisgalleries(List<TblHisgallery> tblHisgalleries) {
		this.tblHisgalleries = tblHisgalleries;
	}

	public TblHisgallery addTblHisgallery(TblHisgallery tblHisgallery) {
		getTblHisgalleries().add(tblHisgallery);
		tblHisgallery.setTblRating(this);

		return tblHisgallery;
	}

	public TblHisgallery removeTblHisgallery(TblHisgallery tblHisgallery) {
		getTblHisgalleries().remove(tblHisgallery);
		tblHisgallery.setTblRating(null);

		return tblHisgallery;
	}

}