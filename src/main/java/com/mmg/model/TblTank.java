package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_tank database table.
 * 
 */
@Entity
@Table(name="tbl_tank")
public class TblTank implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_tank")
	private Long idTank;

	private Long color;

	@Column(name="price_product")
	private Float priceProduct;

	@Column(name="tank_brand")
	private String tankBrand;

	@Column(name="tank_detail")
	private String tankDetail;

	//bi-directional many-to-one association to TblOrderdetail
	@OneToMany(mappedBy="tblTank")
	private List<TblOrderdetail> tblOrderdetails;

	//bi-directional many-to-one association to TblGallery
	@ManyToOne
	@JoinColumn(name="id_gallery")
	private TblGallery tblGallery;

	public TblTank() {
	}

	public Long getIdTank() {
		return this.idTank;
	}

	public void setIdTank(Long idTank) {
		this.idTank = idTank;
	}

	public Long getColor() {
		return this.color;
	}

	public void setColor(Long color) {
		this.color = color;
	}

	public Float getPriceProduct() {
		return this.priceProduct;
	}

	public void setPriceProduct(Float priceProduct) {
		this.priceProduct = priceProduct;
	}

	public String getTankBrand() {
		return this.tankBrand;
	}

	public void setTankBrand(String tankBrand) {
		this.tankBrand = tankBrand;
	}

	public String getTankDetail() {
		return this.tankDetail;
	}

	public void setTankDetail(String tankDetail) {
		this.tankDetail = tankDetail;
	}

	public List<TblOrderdetail> getTblOrderdetails() {
		return this.tblOrderdetails;
	}

	public void setTblOrderdetails(List<TblOrderdetail> tblOrderdetails) {
		this.tblOrderdetails = tblOrderdetails;
	}

	public TblOrderdetail addTblOrderdetail(TblOrderdetail tblOrderdetail) {
		getTblOrderdetails().add(tblOrderdetail);
		tblOrderdetail.setTblTank(this);

		return tblOrderdetail;
	}

	public TblOrderdetail removeTblOrderdetail(TblOrderdetail tblOrderdetail) {
		getTblOrderdetails().remove(tblOrderdetail);
		tblOrderdetail.setTblTank(null);

		return tblOrderdetail;
	}

	public TblGallery getTblGallery() {
		return this.tblGallery;
	}

	public void setTblGallery(TblGallery tblGallery) {
		this.tblGallery = tblGallery;
	}

}