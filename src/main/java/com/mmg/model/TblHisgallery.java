package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the tbl_hisgallery database table.
 * 
 */
@Entity
@Table(name="tbl_hisgallery")
public class TblHisgallery implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_hisgallery")
	private Long idHisgallery;

	@Column(name="detail_hiscar")
	private String detailHiscar;

	//bi-directional many-to-one association to TblGallery
	@ManyToOne
	@JoinColumn(name="id_gallery")
	private TblGallery tblGallery;

	//bi-directional many-to-one association to TblRating
	@ManyToOne
	@JoinColumn(name="id_rc")
	private TblRating tblRating;

	public TblHisgallery() {
	}

	public Long getIdHisgallery() {
		return this.idHisgallery;
	}

	public void setIdHisgallery(Long idHisgallery) {
		this.idHisgallery = idHisgallery;
	}

	public String getDetailHiscar() {
		return this.detailHiscar;
	}

	public void setDetailHiscar(String detailHiscar) {
		this.detailHiscar = detailHiscar;
	}

	public TblGallery getTblGallery() {
		return this.tblGallery;
	}

	public void setTblGallery(TblGallery tblGallery) {
		this.tblGallery = tblGallery;
	}

	public TblRating getTblRating() {
		return this.tblRating;
	}

	public void setTblRating(TblRating tblRating) {
		this.tblRating = tblRating;
	}

}