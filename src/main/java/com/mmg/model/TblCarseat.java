package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_carseat database table.
 * 
 */
@Entity
@Table(name="tbl_carseat")
public class TblCarseat implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_carseat")
	private Long idCarseat;

	@Column(name="carseat_brand")
	private String carseatBrand;

	@Column(name="carseat_detail")
	private String carseatDetail;

	@Column(name="price_product")
	private Float priceProduct;

	//bi-directional many-to-one association to TblGallery
	@ManyToOne
	@JoinColumn(name="id_gallery")
	private TblGallery tblGallery;

	//bi-directional many-to-one association to TblOrderdetail
	@OneToMany(mappedBy="tblCarseat")
	private List<TblOrderdetail> tblOrderdetails;

	public TblCarseat() {
	}

	public Long getIdCarseat() {
		return this.idCarseat;
	}

	public void setIdCarseat(Long idCarseat) {
		this.idCarseat = idCarseat;
	}

	public String getCarseatBrand() {
		return this.carseatBrand;
	}

	public void setCarseatBrand(String carseatBrand) {
		this.carseatBrand = carseatBrand;
	}

	public String getCarseatDetail() {
		return this.carseatDetail;
	}

	public void setCarseatDetail(String carseatDetail) {
		this.carseatDetail = carseatDetail;
	}

	public Float getPriceProduct() {
		return this.priceProduct;
	}

	public void setPriceProduct(Float priceProduct) {
		this.priceProduct = priceProduct;
	}

	public TblGallery getTblGallery() {
		return this.tblGallery;
	}

	public void setTblGallery(TblGallery tblGallery) {
		this.tblGallery = tblGallery;
	}

	public List<TblOrderdetail> getTblOrderdetails() {
		return this.tblOrderdetails;
	}

	public void setTblOrderdetails(List<TblOrderdetail> tblOrderdetails) {
		this.tblOrderdetails = tblOrderdetails;
	}

	public TblOrderdetail addTblOrderdetail(TblOrderdetail tblOrderdetail) {
		getTblOrderdetails().add(tblOrderdetail);
		tblOrderdetail.setTblCarseat(this);

		return tblOrderdetail;
	}

	public TblOrderdetail removeTblOrderdetail(TblOrderdetail tblOrderdetail) {
		getTblOrderdetails().remove(tblOrderdetail);
		tblOrderdetail.setTblCarseat(null);

		return tblOrderdetail;
	}

}