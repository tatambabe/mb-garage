package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the tbl_customer database table.
 * 
 */
@Entity
@Table(name="tbl_customer")
public class TblCustomer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id 
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_member")
	private Long idMember;

	@Column(name="mem_email")
	private String memEmail;

	@Column(name="mem_firstname")
	private String memFirstname;

	@Column(name="mem_gender")
	private String memGender;

	@Column(name="mem_lastname")
	private String memLastname;

	@Column(name="mem_password")
	private String memPassword;

	@Column(name="mem_tel")
	private String memTel;

	@Column(name="mem_username")
	private String memUsername;
	
	@Transient
	private String matchingPassword;

	//bi-directional many-to-one association to TblOrder
	@OneToMany(mappedBy="tblCustomer")
	private List<TblOrder> tblOrders;

	public TblCustomer() {
	}

	public Long getIdMember() {
		return this.idMember;
	}

	public void setIdMember(Long idMember) {
		this.idMember = idMember;
	}

	public String getMemEmail() {
		return this.memEmail;
	}

	public void setMemEmail(String memEmail) {
		this.memEmail = memEmail;
	}

	public String getMemFirstname() {
		return this.memFirstname;
	}

	public void setMemFirstname(String memFirstname) {
		this.memFirstname = memFirstname;
	}

	public String getMemGender() {
		return this.memGender;
	}

	public void setMemGender(String memGender) {
		this.memGender = memGender;
	}

	public String getMemLastname() {
		return this.memLastname;
	}

	public void setMemLastname(String memLastname) {
		this.memLastname = memLastname;
	}

	public String getMemPassword() {
		return this.memPassword;
	}

	public void setMemPassword(String memPassword) {
		this.memPassword = memPassword;
	}

	public String getMemTel() {
		return this.memTel;
	}

	public void setMemTel(String memTel) {
		this.memTel = memTel;
	}

	public String getMemUsername() {
		return this.memUsername;
	}

	public void setMemUsername(String memUsername) {
		this.memUsername = memUsername;
	}

	public List<TblOrder> getTblOrders() {
		return this.tblOrders;
	}

	public void setTblOrders(List<TblOrder> tblOrders) {
		this.tblOrders = tblOrders;
	}

	public TblOrder addTblOrder(TblOrder tblOrder) {
		getTblOrders().add(tblOrder);
		tblOrder.setTblCustomer(this);

		return tblOrder;
	}

	public TblOrder removeTblOrder(TblOrder tblOrder) {
		getTblOrders().remove(tblOrder);
		tblOrder.setTblCustomer(null);

		return tblOrder;
	}

	public String getMatchingPassword() {
		return matchingPassword;
	}

	public void setMatchingPassword(String matchingPassword) {
		this.matchingPassword = matchingPassword;
	}
	

}