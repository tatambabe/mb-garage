package com.mmg.model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the tbl_order database table.
 * 
 */
@Entity
@Table(name="tbl_order")
public class TblOrder implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_order")
	private Long idOrder;

	@Column(name="grand_order")
	private Long grandOrder;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="pay_datetime")
	private Date payDatetime;

	@Column(name="price_order")
	private Float priceOrder;

	//bi-directional many-to-one association to TblCompany
	@ManyToOne
	@JoinColumn(name="id_company")
	private TblCompany tblCompany;

	//bi-directional many-to-one association to TblCustomer
	@ManyToOne
	@JoinColumn(name="id_customer")
	private TblCustomer tblCustomer;

	//bi-directional many-to-one association to TblOrderdetail
	@ManyToOne
	@JoinColumn(name="id_orderdetail")
	private TblOrderdetail tblOrderdetail;

	public TblOrder() {
	}

	public Long getIdOrder() {
		return this.idOrder;
	}

	public void setIdOrder(Long idOrder) {
		this.idOrder = idOrder;
	}

	public Long getGrandOrder() {
		return this.grandOrder;
	}

	public void setGrandOrder(Long grandOrder) {
		this.grandOrder = grandOrder;
	}

	public Date getPayDatetime() {
		return this.payDatetime;
	}

	public void setPayDatetime(Date payDatetime) {
		this.payDatetime = payDatetime;
	}

	public Float getPriceOrder() {
		return this.priceOrder;
	}

	public void setPriceOrder(Float priceOrder) {
		this.priceOrder = priceOrder;
	}

	public TblCompany getTblCompany() {
		return this.tblCompany;
	}

	public void setTblCompany(TblCompany tblCompany) {
		this.tblCompany = tblCompany;
	}

	public TblCustomer getTblCustomer() {
		return this.tblCustomer;
	}

	public void setTblCustomer(TblCustomer tblCustomer) {
		this.tblCustomer = tblCustomer;
	}

	public TblOrderdetail getTblOrderdetail() {
		return this.tblOrderdetail;
	}

	public void setTblOrderdetail(TblOrderdetail tblOrderdetail) {
		this.tblOrderdetail = tblOrderdetail;
	}

}