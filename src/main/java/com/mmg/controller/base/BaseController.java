package com.mmg.controller.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;

import com.mmg.model.TblCustomer;

public abstract class BaseController implements Serializable {

	private static final long serialVersionUID = -4258538592061143589L;
	
	private TblCustomer userLogin;
	private TblCustomer userRegister;
	
	private Boolean isAdmin;
	private String errorMessage;
	private String infoMessage;
	private List<String> genderList;
	
	public void initModelSession(Model model, HttpSession session) throws Exception{
		model.addAttribute("userLogin", getUserLogin());
		model.addAttribute("userRegister", getUserRegister());
		model.addAttribute("isAdmin", getIsAdmin());
		model.addAttribute("errorMessage", getErrorMessage());
		model.addAttribute("infoMessage", getInfoMessage());
		makeGenderList();
		model.addAttribute("genderList", genderList);
	}
	
	public void clearSession(HttpSession session, String attrName) throws Exception {
		TblCustomer customerSession = (TblCustomer) session.getAttribute(attrName);
		if (customerSession != null) {
			session.removeAttribute(attrName);
		}
	}
	
	public void clearAll() throws Exception {
		userLogin = null;
		userRegister = null;
		isAdmin = Boolean.FALSE;
		errorMessage = null;
		infoMessage = null;
	}
	
	public void makeGenderList() {
		if (getGenderList()!=null && getGenderList().size()>0) {
			//not do anything
		} else {
			getGenderList().add("Male");
			getGenderList().add("Female");
			getGenderList().add("Other");
		}
	}
	
	public TblCustomer getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(TblCustomer userLogin) {
		this.userLogin = userLogin;
	}

	public Boolean getIsAdmin() {
		if (isAdmin == null) {
			isAdmin = Boolean.FALSE;
		}
		return isAdmin;
	}

	public void setIsAdmin(Boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getInfoMessage() {
		return infoMessage;
	}

	public void setInfoMessage(String infoMessage) {
		this.infoMessage = infoMessage;
	}

	public TblCustomer getUserRegister() {
		if (userRegister == null) {
			userRegister = new TblCustomer();
		}
		return userRegister;
	}

	public void setUserRegister(TblCustomer userRegister) {
		this.userRegister = userRegister;
	}

	public List<String> getGenderList() {
		if (genderList == null) {
			genderList = new ArrayList<String>();
		}
		return genderList;
	}

	public void setGenderList(List<String> genderList) {
		this.genderList = genderList;
	}
	
	
}
