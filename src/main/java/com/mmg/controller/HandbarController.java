package com.mmg.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mmg.controller.base.BaseController;
import com.mmg.model.TblHandbar;


@Controller
@EnableAutoConfiguration
@RequestMapping("/manage")
public class HandbarController extends BaseController{

	private static final long serialVersionUID = 7533512529814232458L;
	
	private static Logger log = LoggerFactory.getLogger(HandbarController.class);

	@RequestMapping(value="/handbar", method = RequestMethod.GET)
	public String getHandbarAll(Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/handbar";
	}
	
	@RequestMapping(value="/handbar/{id}", method = RequestMethod.GET)
	public String getHandbarById(@PathVariable("id") Long idPk, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/handbar-info";
	}
	
	@RequestMapping(value="/handbar/{id}", method = RequestMethod.POST)
	public String updateHandbar(@PathVariable("id") Long idPk, @RequestBody TblHandbar handbar, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "handbar";
	}
	
	@RequestMapping(value="/handbar", method = RequestMethod.POST)
	public String insertHandbar(@RequestBody TblHandbar handbar, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/handbar";
	}
}
