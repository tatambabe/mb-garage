package com.mmg.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mmg.controller.base.BaseController;
import com.mmg.model.TblWheel;


@Controller
@EnableAutoConfiguration
@RequestMapping("/manage")
public class WheelController extends BaseController{

	private static final long serialVersionUID = 308821460441886286L;
	
	private static Logger log = LoggerFactory.getLogger(WheelController.class);

	@RequestMapping(value="/wheel", method = RequestMethod.GET)
	public String getWheelAll(Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/wheel";
	}
	
	@RequestMapping(value="/wheel/{id}", method = RequestMethod.GET)
	public String getWheelById(@PathVariable("id") Long idPk, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/wheel-info";
	}
	
	@RequestMapping(value="/wheel/{id}", method = RequestMethod.POST)
	public String updateWheel(@PathVariable("id") Long idPk, @RequestBody TblWheel wheel, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/wheel";
	}
	
	@RequestMapping(value="/wheel", method = RequestMethod.POST)
	public String insertWheel(@RequestBody TblWheel wheel, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/wheel";
	}
}
