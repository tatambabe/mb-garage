package com.mmg.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mmg.controller.base.BaseController;
import com.mmg.model.TblCompany;

@Controller
@EnableAutoConfiguration
@RequestMapping("/manage")
public class CompanyController extends BaseController {

	private static final long serialVersionUID = 6900023629383853588L;

	private static Logger log = LoggerFactory.getLogger(CompanyController.class);

	@RequestMapping(value = "/company", method = RequestMethod.GET)
	public String getCompanyAll(Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/company";
	}

	@RequestMapping(value = "/company/{id}", method = RequestMethod.GET)
	public String getCustomerById(@PathVariable("id") Long idPk, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/company-info";
	}

	@RequestMapping(value = "/company/{id}", method = RequestMethod.POST)
	public String updateCompany(@PathVariable("id") Long idPk, @RequestBody TblCompany company, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/customer";
	}

	@RequestMapping(value = "/company", method = RequestMethod.POST)
	public String insertCompany(@RequestBody TblCompany company, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/company";
	}
}
