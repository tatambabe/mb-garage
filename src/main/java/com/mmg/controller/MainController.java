package com.mmg.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.mmg.controller.base.BaseController;
import com.mmg.model.TblCustomer;
import com.mmg.service.ICustomerService;

@Controller
public class MainController extends BaseController {

	private static final long serialVersionUID = 7652087773957961241L;
	
	private static Logger log = LoggerFactory.getLogger(MainController.class);

	@Autowired
	ICustomerService customerService;
	
	@ModelAttribute("userRegister")
	public TblCustomer userRegister() {
		return getUserRegister();
	}

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Model model, HttpSession session) throws Exception{
		log.info("call method home");
		initModelSession(model, session);
		return "index";
	}

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String Login(Model model, HttpSession session) throws Exception {
		log.info("call method login");
		initModelSession(model, session);
		return "index";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String doLogin(@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "password", required = true) String password, HttpSession session) throws Exception {
		log.info("call method doLogin, username:" + username + ",password:" + password);
		clearAll();
		try {
			// check value
			if (StringUtils.isEmpty(username) && StringUtils.isEmpty(password)) {
				log.info("Parameters is null, username, password");
				setErrorMessage("Parameters is null, username, password");
			} else {
				// search
				TblCustomer customer = customerService.findByUserName(username);
				if (customer != null) {
					String pwd = customer.getMemPassword();
					if (password.trim().equals(pwd)) {
						log.info("Username:" + username + " is verified!!!!");
						setUserLogin(customer); //set user 
						TblCustomer customerSession = (TblCustomer) session.getAttribute("USER_SESSION");
						if (customerSession == null) {
							session.setAttribute("USER_SESSION_" + customer.getMemUsername() , customer);
						}
						if ("admin".equals(customer.getMemUsername())) {
							setIsAdmin(Boolean.TRUE); //set flag Admin 
						}
						setInfoMessage(customer.getMemFirstname() + " " + customer.getMemLastname());
						
					} else {
						log.info("Username:" + username + " is not verified!!!!");
						setErrorMessage("Username:" + username + " is not verified!!!!");
					}
				} else {
					log.info("Username:" + username + " not found.");
					setErrorMessage("Username:" + username + " not found!!!!");
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		}
		if (getErrorMessage()!=null) {
			return "redirect:/login?error";
		}
		return "redirect:/login?info";
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String goHome(Model model, HttpSession session) throws Exception{
		log.info("call method goHome");
		initModelSession(model, session);
		return "redirect:";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public String Logout(HttpSession session) throws Exception {
		log.info("call method logout");
		TblCustomer customer = getUserLogin();
		if (customer!=null) {
			clearSession(session, "USER_SESSION" + customer.getMemUsername());
		}
		clearAll();
		return "redirect:login?logout";
	}

	@RequestMapping(value = "/custombike", method = RequestMethod.GET)
	public String customerBike(Model model, HttpSession session) throws Exception{
		log.info("call method customBike");
		initModelSession(model, session);
		return "custombike";
	}

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String getRegister(Model model, HttpSession session) throws Exception{
		log.info("call method getRegister");
		initModelSession(model, session);
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String postRegister(@ModelAttribute("userRegister") @Valid TblCustomer userRegister, BindingResult result, HttpSession session) throws Exception {
		log.info("call method postRegister");
		String redirectName = "login?registererror";
		// call insert
		clearAll();
		try {
			TblCustomer checkUser = customerService.findByUserName(userRegister.getMemUsername());
			if (checkUser!=null) {
				setErrorMessage("Duplicate user-name:" + userRegister.getMemUsername());
			} else {
				TblCustomer resultSave = customerService.save(userRegister);
				setUserLogin(resultSave);
				if (resultSave != null) {
					session.setAttribute("USER_SESSION_" + resultSave.getMemUsername() , resultSave);
					setInfoMessage(resultSave.getMemFirstname() + " " + resultSave.getMemLastname());
					redirectName = "redirect:login?registersuccess";
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			setErrorMessage("Exception :" + e.getMessage());
		}
		return redirectName;
	}

}
