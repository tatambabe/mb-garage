package com.mmg.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mmg.controller.base.BaseController;
import com.mmg.model.TblCarseat;

@Controller
@EnableAutoConfiguration
@RequestMapping("/manage")
public class CarseatController extends BaseController {

	private static final long serialVersionUID = 6041045122147095856L;

	private static Logger log = LoggerFactory.getLogger(CarseatController.class);

	@RequestMapping(value = "/carseat", method = RequestMethod.GET)
	public String getCarseatAll(Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/carseat";
	}

	@RequestMapping(value = "/carseat/{id}", method = RequestMethod.GET)
	public String getCarseatById(@PathVariable("id") Long idPk, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/carseat-info";
	}

	@RequestMapping(value = "/carseat/{id}", method = RequestMethod.POST)
	public String updateCarseat(@PathVariable("id") Long idPk, @RequestBody TblCarseat carseat, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/carseat";
	}

	@RequestMapping(value = "/carseat", method = RequestMethod.POST)
	public String insertCustomer(@RequestBody TblCarseat carseat, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/carseat";
	}
}
