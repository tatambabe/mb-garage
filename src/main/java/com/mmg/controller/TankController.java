package com.mmg.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mmg.controller.base.BaseController;
import com.mmg.model.TblTank;


@Controller
@EnableAutoConfiguration
@RequestMapping("/manage")
public class TankController extends BaseController{

	private static final long serialVersionUID = -931314819295276187L;
	
	private static Logger log = LoggerFactory.getLogger(TankController.class);

	@RequestMapping(value="/tank", method = RequestMethod.GET)
	public String getTankAll(Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/tank";
	}
	
	@RequestMapping(value="/tank/{id}", method = RequestMethod.GET)
	public String getTankById(@PathVariable("id") Long idPk, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/tank-info";
	}
	
	@RequestMapping(value="/tank/{id}", method = RequestMethod.POST)
	public String updateTank(@PathVariable("id") Long idPk, @RequestBody TblTank tank, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/tank";
	}
	
	@RequestMapping(value="/tank", method = RequestMethod.POST)
	public String insertTank(@RequestBody TblTank tank, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/tank";
	}
}
