package com.mmg.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mmg.controller.base.BaseController;
import com.mmg.model.TblCustomer;

@Controller
@EnableAutoConfiguration
@RequestMapping("/manage")
public class CustomerController extends BaseController {

	private static final long serialVersionUID = 6488041516483172546L;

	private static Logger log = LoggerFactory.getLogger(CustomerController.class);

	@RequestMapping(value = "/customer", method = RequestMethod.GET)
	public String getCustomerAll(Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/customer";
	}

	@RequestMapping(value = "/customer/{id}", method = RequestMethod.GET)
	public String getCustomerById(@PathVariable("id") Long idPk, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/customer-info";
	}

	@RequestMapping(value = "/customer/{id}", method = RequestMethod.POST)
	public String updateCustomer(@PathVariable("id") Long idPk, @RequestBody TblCustomer customer, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/customer";
	}

	@RequestMapping(value = "/customer", method = RequestMethod.POST)
	public String insertCustomer(@RequestBody TblCustomer customer, Model model, HttpSession session) throws Exception {
		initModelSession(model, session);
		return "manage/customer";
	}
}
