package com.mmg.service;

import java.util.List;
import java.util.Optional;

import com.mmg.model.TblCarseat;

public interface ICarseatService {
		public List<TblCarseat> findAll() throws Exception;
		public TblCarseat save(TblCarseat bean) throws Exception;
		public Optional<TblCarseat> findById(Long id) throws Exception;

}
