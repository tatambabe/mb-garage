package com.mmg.service;

import java.util.List;
import java.util.Optional;

import com.mmg.model.TblCompany;

public interface ICompanyService {
		public List<TblCompany> findAll() throws Exception;
		public TblCompany save(TblCompany bean) throws Exception;
		public Optional<TblCompany> findById(Long id) throws Exception;

}
