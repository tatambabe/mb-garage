package com.mmg.service;

import java.util.List;
import java.util.Optional;

import com.mmg.model.TblGallery;

public interface IGalleryService {
		public List<TblGallery> findAll() throws Exception;
		public TblGallery save(TblGallery bean) throws Exception;
		public Optional<TblGallery> findById(Long id) throws Exception;

}
