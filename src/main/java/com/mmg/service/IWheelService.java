package com.mmg.service;

import java.util.List;
import java.util.Optional;

import com.mmg.model.TblWheel;

public interface IWheelService {
		public List<TblWheel> findAll() throws Exception;
		public TblWheel save(TblWheel bean) throws Exception;
		public Optional<TblWheel> findById(Long id) throws Exception;

}
