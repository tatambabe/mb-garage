package com.mmg.service;

import java.util.List;
import java.util.Optional;

import com.mmg.model.TblHisgallery;

public interface IHisGalleryService {
		public List<TblHisgallery> findAll() throws Exception;
		public TblHisgallery save(TblHisgallery bean) throws Exception;
		public Optional<TblHisgallery> findById(Long id) throws Exception;

}
