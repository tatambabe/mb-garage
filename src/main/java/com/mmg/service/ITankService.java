package com.mmg.service;

import java.util.List;
import java.util.Optional;

import com.mmg.model.TblTank;

public interface ITankService {
		public List<TblTank> findAll() throws Exception;
		public TblTank save(TblTank bean) throws Exception;
		public Optional<TblTank> findById(Long id) throws Exception;

}
