package com.mmg.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mmg.model.TblCompany;
import com.mmg.repository.TblCompanyRepository;
import com.mmg.service.ICompanyService;

@Service
@Transactional(rollbackFor = Throwable.class)
public class CompanyServiceImpl implements ICompanyService {

	@Autowired
	TblCompanyRepository companyRepo;
	
	@Override
	public List<TblCompany> findAll() throws Exception {
		return (List<TblCompany>) companyRepo.findAll();
	}

	@Transactional
	@Override
	public TblCompany save(TblCompany bean) throws Exception {
		return (TblCompany) companyRepo.save(bean);
	}

	@Override
	public Optional<TblCompany> findById(Long id) throws Exception {
		return (Optional<TblCompany>) companyRepo.findById(id);
	}

}
