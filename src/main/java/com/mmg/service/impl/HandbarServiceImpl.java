package com.mmg.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mmg.model.TblHandbar;
import com.mmg.repository.TblHandbarRepository;
import com.mmg.service.IHandbarService;

@Service
@Transactional(rollbackFor = Throwable.class)
public class HandbarServiceImpl implements IHandbarService {

	@Autowired
	TblHandbarRepository handbarRepo;
	
	@Override
	public List<TblHandbar> findAll() throws Exception {
		return (List<TblHandbar>) handbarRepo.findAll();
	}

	@Transactional
	@Override
	public TblHandbar save(TblHandbar bean) throws Exception {
		return (TblHandbar) handbarRepo.save(bean);
	}

	@Override
	public Optional<TblHandbar> findById(Long id) throws Exception {
		return (Optional<TblHandbar>) handbarRepo.findById(id);
	}

}
