package com.mmg.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mmg.model.TblGallery;
import com.mmg.repository.TblGalleryRepository;
import com.mmg.service.IGalleryService;

@Service
@Transactional(rollbackFor = Throwable.class)
public class GalleryServiceImpl implements IGalleryService {

	@Autowired
	TblGalleryRepository galleryRepo;
	
	@Override
	public List<TblGallery> findAll() throws Exception {
		return (List<TblGallery>) galleryRepo.findAll();
	}

	@Transactional
	@Override
	public TblGallery save(TblGallery bean) throws Exception {
		return (TblGallery) galleryRepo.save(bean);
	}

	@Override
	public Optional<TblGallery> findById(Long id) throws Exception {
		return (Optional<TblGallery>) galleryRepo.findById(id);
	}

}
