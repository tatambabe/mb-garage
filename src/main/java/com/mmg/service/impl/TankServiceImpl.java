package com.mmg.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mmg.model.TblTank;
import com.mmg.repository.TblTankRepository;
import com.mmg.service.ITankService;

@Service
@Transactional(rollbackFor = Throwable.class)
public class TankServiceImpl implements ITankService {

	@Autowired
	TblTankRepository tankRepo;
	
	@Override
	public List<TblTank> findAll() throws Exception {
		return (List<TblTank>) tankRepo.findAll();
	}

	@Transactional
	@Override
	public TblTank save(TblTank bean) throws Exception {
		return (TblTank) tankRepo.save(bean);
	}

	@Override
	public Optional<TblTank> findById(Long id) throws Exception {
		return (Optional<TblTank>) tankRepo.findById(id);
	}

}
