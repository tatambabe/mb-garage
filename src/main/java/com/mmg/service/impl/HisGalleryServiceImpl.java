package com.mmg.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mmg.model.TblHisgallery;
import com.mmg.repository.TblHisgalleryRepository;
import com.mmg.service.IHisGalleryService;

@Service
@Transactional(rollbackFor = Throwable.class)
public class HisGalleryServiceImpl implements IHisGalleryService {

	@Autowired
	TblHisgalleryRepository hisgalleryRepo;
	
	@Override
	public List<TblHisgallery> findAll() throws Exception {
		return (List<TblHisgallery>) hisgalleryRepo.findAll();
	}

	@Transactional
	@Override
	public TblHisgallery save(TblHisgallery bean) throws Exception {
		return (TblHisgallery) hisgalleryRepo.save(bean);
	}

	@Override
	public Optional<TblHisgallery> findById(Long id) throws Exception {
		return (Optional<TblHisgallery>) hisgalleryRepo.findById(id);
	}

}
