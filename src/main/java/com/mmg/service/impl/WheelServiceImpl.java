package com.mmg.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mmg.model.TblWheel;
import com.mmg.repository.TblWheelRepository;
import com.mmg.service.IWheelService;

@Service
@Transactional(rollbackFor = Throwable.class)
public class WheelServiceImpl implements IWheelService {

	@Autowired
	TblWheelRepository wheelRepo;
	
	@Override
	public List<TblWheel> findAll() throws Exception {
		return (List<TblWheel>) wheelRepo.findAll();
	}

	@Transactional
	@Override
	public TblWheel save(TblWheel bean) throws Exception {
		return (TblWheel) wheelRepo.save(bean);
	}

	@Override
	public Optional<TblWheel> findById(Long id) throws Exception {
		return (Optional<TblWheel>) wheelRepo.findById(id);
	}

}
