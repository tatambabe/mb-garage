package com.mmg.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mmg.model.TblCarseat;
import com.mmg.repository.TblCarseatRepository;
import com.mmg.service.ICarseatService;

@Service
@Transactional(rollbackFor = Throwable.class)
public class CarseatServiceIml implements ICarseatService {

	@Autowired
	TblCarseatRepository carseatRepo;
	
	@Override
	public List<TblCarseat> findAll() throws Exception {
		return (List<TblCarseat>) carseatRepo.findAll();
	}

	@Override
	@Transactional
	public TblCarseat save(TblCarseat bean) throws Exception {
		return (TblCarseat) carseatRepo.save(bean);
	}

	@Override
	public Optional<TblCarseat> findById(Long id) throws Exception {
		return (Optional<TblCarseat>) carseatRepo.findById(id);
	}

}
