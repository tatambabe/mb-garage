package com.mmg.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mmg.model.TblCustomer;
import com.mmg.repository.CustomerRepository;
import com.mmg.repository.TblCustomerRepository;
import com.mmg.service.ICustomerService;

@Service
@Transactional(rollbackFor = Throwable.class)
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	TblCustomerRepository tblCustomerRepo;
	
	@Autowired
	CustomerRepository customerRepo;
	
	@Override
	public List<TblCustomer> findAll() throws Exception {
		return (List<TblCustomer>) tblCustomerRepo.findAll();
	}

	@Transactional
	@Override
	public TblCustomer save(TblCustomer bean) throws Exception {
		return (TblCustomer) tblCustomerRepo.save(bean);
	}

	@Override
	public Optional<TblCustomer> findById(Long id) throws Exception {
		return (Optional<TblCustomer>) tblCustomerRepo.findById(id);
	}

	@Override
	public TblCustomer findByUserName(String username) throws Exception {
		return (TblCustomer) customerRepo.findByUserName(username);
	}

}
