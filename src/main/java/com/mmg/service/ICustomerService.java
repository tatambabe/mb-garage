package com.mmg.service;

import java.util.List;
import java.util.Optional;

import com.mmg.model.TblCustomer;

public interface ICustomerService {
		public List<TblCustomer> findAll() throws Exception;
		public TblCustomer save(TblCustomer bean) throws Exception;
		public Optional<TblCustomer> findById(Long id) throws Exception;
		public TblCustomer findByUserName(String username) throws Exception;

}
