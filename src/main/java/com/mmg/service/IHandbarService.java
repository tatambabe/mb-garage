package com.mmg.service;

import java.util.List;
import java.util.Optional;

import com.mmg.model.TblHandbar;

public interface IHandbarService {
		public List<TblHandbar> findAll() throws Exception;
		public TblHandbar save(TblHandbar bean) throws Exception;
		public Optional<TblHandbar> findById(Long id) throws Exception;

}
