package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblOrder;

public interface TblOrderRepository extends CrudRepository<TblOrder, Long> {
	
	List<TblOrder> findByIdOrder(Long idOrder) throws Exception;

}
