package com.mmg.repository;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.mmg.model.TblCustomer;
import com.mmg.repository.base.BaseRepository;

@Repository
public class CustomerRepository extends BaseRepository<Object> implements Serializable{

	private static final long serialVersionUID = 1879822486137619455L;

	public TblCustomer findByUserName(String username) throws Exception {
		/**
		 * select t.* from tbl_customer t where t.mem_username = :username
		 */
		TblCustomer result = null;
		try {
			String sql = "select t.* from tbl_customer t where t.mem_username = :username";
			Query query = (Query) getEntityManager().createNativeQuery(sql, TblCustomer.class);
			query.setParameter("username", username);
			List<TblCustomer> list = (List<TblCustomer>) query.getResultList();
		    if(list !=null && list.size()>0) {
		    	result = list.get(0);
		    }
		} catch(Exception e) {
			e.printStackTrace();
		}
		return result;
	}
}
