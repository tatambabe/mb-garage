package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblOrderdetail;

public interface TblOrderdetailRepository extends CrudRepository<TblOrderdetail, Long> {
	
	List<TblOrderdetail> findByIdOrderdetail(Long idOrderdetail) throws Exception;

}
