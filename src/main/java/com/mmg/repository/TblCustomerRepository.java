package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblCustomer;

public interface TblCustomerRepository extends CrudRepository<TblCustomer, Long> {
	
	List<TblCustomer> findByIdMember(Long idMember) throws Exception;
	
}
