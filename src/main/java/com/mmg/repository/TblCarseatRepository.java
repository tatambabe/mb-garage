package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblCarseat;

public interface TblCarseatRepository extends CrudRepository<TblCarseat, Long> {
	
	List<TblCarseat> findByIdCarseat(Long idCarseat) throws Exception;

}
