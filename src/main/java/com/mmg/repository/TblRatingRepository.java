package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblRating;

public interface TblRatingRepository extends CrudRepository<TblRating, Long> {
	
	List<TblRating> findByIdRc(Long idRc) throws Exception;

}
