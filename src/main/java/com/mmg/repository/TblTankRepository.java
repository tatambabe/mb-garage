package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblTank;

public interface TblTankRepository extends CrudRepository<TblTank, Long> {
	
	List<TblTank> findByIdTank(Long idTank) throws Exception;

}
