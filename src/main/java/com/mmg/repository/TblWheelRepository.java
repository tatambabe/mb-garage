package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblWheel;


public interface TblWheelRepository extends CrudRepository<TblWheel, Long> {
	
	List<TblWheel> findByIdWheel(Long idWheel) throws Exception;

}
