package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblHandbar;

public interface TblHandbarRepository extends CrudRepository<TblHandbar, Long> {
	
	List<TblHandbar> findByIdHandbar(Long idHandbar) throws Exception;

}
