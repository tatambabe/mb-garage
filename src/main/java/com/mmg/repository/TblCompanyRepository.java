package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblCompany;

public interface TblCompanyRepository extends CrudRepository<TblCompany, Long> {
	
	List<TblCompany> findByIdCompany(Long idCompany) throws Exception;

}
