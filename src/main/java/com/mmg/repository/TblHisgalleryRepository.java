package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblHisgallery;

public interface TblHisgalleryRepository extends CrudRepository<TblHisgallery, Long> {
	
	List<TblHisgallery> findByIdHisgallery(Long idHisgallery) throws Exception;

}
