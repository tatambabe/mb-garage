package com.mmg.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mmg.model.TblGallery;

public interface TblGalleryRepository extends CrudRepository<TblGallery, Long> {
	
	List<TblGallery> findByIdGallery(Long idGallery) throws Exception;

}
