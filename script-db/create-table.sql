CREATE TABLE `tbl_company` (
  `id_company` int(11) NOT NULL AUTO_INCREMENT,
  `name_company` varchar(250) DEFAULT NULL,
  `address_company` varchar(500) DEFAULT NULL,
  `tel_company` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางเก็บข้อมูลร้าน'
;

CREATE TABLE `tbl_customer` (
  `id_member` int(11) NOT NULL AUTO_INCREMENT,
  `mem_gender` varchar(11) DEFAULT NULL,
  `mem_firstname` varchar(50) DEFAULT NULL,
  `mem_lastname` varchar(50) DEFAULT NULL,
  `mem_tel` varchar(11) DEFAULT NULL,
  `mem_email` varchar(100) DEFAULT NULL,
  `mem_username` varchar(20) DEFAULT NULL,
  `mem_password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_member`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางข้อมูลลูกค้า'
;

CREATE TABLE `tbl_gallery` (
  `id_gallery` int(11) NOT NULL AUTO_INCREMENT,
  `pic_name` varchar(50) DEFAULT NULL,
  `pic_type` varchar(10) DEFAULT NULL,
  `pic_path` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id_gallery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางเก็บรูปภาพ'
;

CREATE TABLE `tbl_rating` (
  `id_rc` int(11) NOT NULL AUTO_INCREMENT,
  `rc_rating` decimal(3,2) DEFAULT NULL,
  `rc_comment` varchar(250) DEFAULT NULL,
  `rc_datetime` datetime DEFAULT NULL,
  PRIMARY KEY (`id_rc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางประเมิณร้าน'
;

CREATE TABLE `tbl_carseat` (
  `id_carseat` int(11) NOT NULL AUTO_INCREMENT,
  `carseat_brand` varchar(50) DEFAULT NULL,
  `carseat_detail` varchar(50) DEFAULT NULL,
  `price_product` float DEFAULT NULL,
  `id_gallery` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_carseat`),
  KEY `tbl_carseat_fk` (`id_gallery`),
  CONSTRAINT `tbl_carseat_fk` FOREIGN KEY (`id_gallery`) REFERENCES `tbl_gallery` (`id_gallery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางออกแบบเบาะ'
;

CREATE TABLE `tbl_handbar` (
  `id_handbar` int(11) NOT NULL AUTO_INCREMENT,
  `hand_detail` varchar(50) DEFAULT NULL,
  `hand_brand` varchar(50) DEFAULT NULL,
  `price_product` float DEFAULT NULL,
  `id_gallery` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_handbar`),
  KEY `tbl_handbar_fk` (`id_gallery`),
  CONSTRAINT `tbl_handbar_fk` FOREIGN KEY (`id_gallery`) REFERENCES `tbl_gallery` (`id_gallery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางออกแบบแฮนด์'
;

CREATE TABLE `tbl_hisgallery` (
  `id_hisgallery` int(11) NOT NULL AUTO_INCREMENT,
  `detail_hiscar` varchar(500) DEFAULT NULL,
  `id_rc` int(11) DEFAULT NULL,
  `id_gallery` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_hisgallery`),
  KEY `tbl_hisgallery_fk` (`id_rc`),
  KEY `tbl_hisgallery_fk_1` (`id_gallery`),
  CONSTRAINT `tbl_hisgallery_fk` FOREIGN KEY (`id_rc`) REFERENCES `tbl_rating` (`id_rc`),
  CONSTRAINT `tbl_hisgallery_fk_1` FOREIGN KEY (`id_gallery`) REFERENCES `tbl_gallery` (`id_gallery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางเก็บรถเก่า'
;

CREATE TABLE `tbl_tank` (
  `id_tank` int(11) NOT NULL AUTO_INCREMENT,
  `tank_detail` varchar(250) DEFAULT NULL,
  `tank_brand` varchar(250) DEFAULT NULL,
  `price_product` float DEFAULT NULL,
  `color` int(11) DEFAULT NULL,
  `id_gallery` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_tank`),
  KEY `tbl_tank_fk` (`id_gallery`),
  CONSTRAINT `tbl_tank_fk` FOREIGN KEY (`id_gallery`) REFERENCES `tbl_gallery` (`id_gallery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางเก็บถัง'
;

CREATE TABLE `tbl_wheel` (
  `id_wheel` int(11) NOT NULL AUTO_INCREMENT,
  `wheel_size` int(11) DEFAULT NULL,
  `wheel_brand` varchar(50) DEFAULT NULL,
  `price_product` float DEFAULT NULL,
  `id_gallery` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_wheel`),
  KEY `tbl_wheel_fk` (`id_gallery`),
  CONSTRAINT `tbl_wheel_fk` FOREIGN KEY (`id_gallery`) REFERENCES `tbl_gallery` (`id_gallery`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางออกแบบล้อ'
;

CREATE TABLE `tbl_orderdetail` (
  `id_orderdetail` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(20) DEFAULT NULL,
  `pay_id` varchar(20) DEFAULT NULL,
  `quantity_order` varchar(5) DEFAULT NULL,
  `id_tank` int(11) DEFAULT NULL,
  `id_handbar` int(11) DEFAULT NULL,
  `id_carseat` int(11) DEFAULT NULL,
  `id_wheel` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_orderdetail`),
  KEY `tbl_orderdetail_fk` (`id_tank`),
  KEY `tbl_orderdetail_fk_1` (`id_handbar`),
  KEY `tbl_orderdetail_fk_2` (`id_carseat`),
  KEY `tbl_orderdetail_fk_3` (`id_wheel`),
  CONSTRAINT `tbl_orderdetail_fk` FOREIGN KEY (`id_tank`) REFERENCES `tbl_tank` (`id_tank`),
  CONSTRAINT `tbl_orderdetail_fk_1` FOREIGN KEY (`id_handbar`) REFERENCES `tbl_handbar` (`id_handbar`),
  CONSTRAINT `tbl_orderdetail_fk_2` FOREIGN KEY (`id_carseat`) REFERENCES `tbl_carseat` (`id_carseat`),
  CONSTRAINT `tbl_orderdetail_fk_3` FOREIGN KEY (`id_wheel`) REFERENCES `tbl_wheel` (`id_wheel`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางข้อมูลการสั่งซื้อ'
;

CREATE TABLE `tbl_order` (
  `id_order` int(11) NOT NULL AUTO_INCREMENT,
  `price_order` float NOT NULL,
  `pay_datetime` datetime DEFAULT NULL,
  `grand_order` int(11) DEFAULT NULL,
  `id_orderdetail` int(11) DEFAULT NULL,
  `id_customer` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_order`),
  KEY `tbl_order_fk` (`id_orderdetail`),
  KEY `tbl_order_fk_1` (`id_customer`),
  KEY `tbl_order_fk_2` (`id_company`),
  CONSTRAINT `tbl_order_fk` FOREIGN KEY (`id_orderdetail`) REFERENCES `tbl_orderdetail` (`id_orderdetail`),
  CONSTRAINT `tbl_order_fk_1` FOREIGN KEY (`id_customer`) REFERENCES `tbl_customer` (`id_member`),
  CONSTRAINT `tbl_order_fk_2` FOREIGN KEY (`id_company`) REFERENCES `tbl_company` (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='ตารางผู้ขาย'
;